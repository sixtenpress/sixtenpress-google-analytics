=== Six/Ten Press Google Analytics ===

Contributors: littler.chicken
Donate link: https://robincornett.com/donate/
Tags: google analytics, google tag manager
Requires at least: 4.8
Tested up to: 5.1
Stable tag: 0.1.0
License: GPL-2.0+
License URI: http://www.gnu.org/licenses/gpl-2.0.txt

This plugin easily adds the Google Tag Manager scripts to your website.

== Description ==

_Six/Ten Press Google Analytics_ is a wee plugin to add Google Tag Manager/Google Analytics quickly and easily to your website.

== Installation ==

1. Upload the entire `sixtenpress-google-analytics` folder to your `/wp-content/plugins` directory.
1. Activate the plugin through the 'Plugins' menu in WordPress.
1. Visit the Settings > Six/Ten Press Google Analytics page to change the default behavior of the plugin.

== Frequently Asked Questions ==

== Screenshots ==

== Upgrade Notice ==

== Changelog ==

= 0.1.0 =
* Initial release