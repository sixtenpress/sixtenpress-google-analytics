<?php
/**
 * Copyright (c) 2019 Robin Cornett
 */

class SixTenPressGoogleAnalyticsSettings extends SixTenPressSettings {

	/**
	 * String for the page/section/setting.
	 * @var string $page
	 */
	protected $page = 'sixtenpress';

	/**
	 * String for the tab.
	 * @var string
	 */
	protected $tab = 'sixtenpressgoogleanalytics';

	/**
	 * Define the setting key.
	 * @return array
	 */
	protected function define_setting() {
		return apply_filters(
			'sixtenpressgoogleanalytics_settings_defaults',
			array(
				'tracking_id' => '',
			)
		);
	}

	/**
	 * Maybe add a new settings page.
	 */
	public function maybe_add_settings_page() {
		$this->setting = $this->get_setting();
		add_action( 'admin_init', array( $this, 'register' ) );

		if ( ! $this->is_sixten_active() ) {
			$this->page = $this->tab;
			add_options_page(
				__( '6/10 Press Google Analytics Settings', 'sixtenpress' ),
				__( '6/10 Press Google Analytics', 'sixtenpress' ),
				'manage_options',
				$this->page,
				array( $this, 'do_simple_settings_form' )
			);
		}
		$this->action = "{$this->page}_save-settings";
		$this->nonce  = "{$this->page}_nonce";

		add_filter( 'sixtenpress_settings_tabs', array( $this, 'add_tab' ) );
		add_action( "load-settings_page_{$this->page}", array( $this, 'build_settings_page' ) );
	}

	/**
	 * Add the sections and settings to the settings tab/page.
	 */
	public function build_settings_page() {
		$sections = $this->define_section();
		$this->add_sections( $sections );
		$this->add_fields( $this->register_fields(), $sections );
	}

	/**
	 * Add a tab to the 6/10 Press settings page.
	 *
	 * @param $tabs
	 *
	 * @return array
	 */
	public function add_tab( $tabs ) {
		$tabs[] = array(
			'id'  => 'googleanalytics',
			'tab' => __( 'Google Analytics', 'sixtenpress-google-analytics' ),
		);

		return $tabs;
	}

	/**
	 * Create a settings section if 6/10 press is not active.
	 * @return array
	 */
	public function define_section() {
		return array(
			'googleanalytics' => array(
				'id'    => 'googleanalytics',
				'tab'   => 'googleanalytics',
				'title' => __( 'Google Analytics Settings', 'sixtenpress-google-analytics' ),
			),
		);
	}

	/**
	 * Print the section description.
	 */
	public function googleanalytics_section_description() {
		echo '<p>' . esc_html__( '6/10 Press Google Analytics has just one setting: the tracking ID. Administrators and editors are excluded from tracking.', 'sixtenpress-google-analytics' ) . '</p>';
	}

	/**
	 * Define the settings fields.
	 *
	 * @return array
	 */
	protected function register_fields() {
		return apply_filters(
			'sixtenpressgoogleanalytics_settings_fields',
			array(
				array(
					'id'      => 'tracking_id',
					'title'   => __( 'Google Analytics', 'sixtenpress-google-analytics' ),
					'type'    => 'text',
					'section' => 'googleanalytics',
				),
			)
		);
	}
}
