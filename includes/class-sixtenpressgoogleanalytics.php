<?php

/**
 * Class SixTenPressGoogleAnalytics
 */
class SixTenPressGoogleAnalytics {

	/**
	 * The Google Analytics tracking ID.
	 *
	 * @var $tracking_id string
	 */
	protected $tracking_id;

	/**
	 * SixTenPressGoogleAnalytics constructor.
	 */
	public function __construct() {
		add_action( 'plugins_loaded', array( $this, 'load_settings' ), 20 );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue' ) );
		add_filter( 'script_loader_tag', array( $this, 'async' ), 10, 2 );
	}

	/**
	 * Check for settings class and create the settings page.
	 */
	public function load_settings() {
		if ( ! class_exists( 'SixTenPressSettings' ) ) {
			include_once 'common/class-sixtenpress-settings.php';
		}
		include_once 'class-sixtenpressgoogleanalytics-settings.php';

		$settings = new SixTenPressGoogleAnalyticsSettings();
		add_action( 'admin_menu', array( $settings, 'maybe_add_settings_page' ), 20 );
	}

	/**
	 * Enqueue the scripts if the current user is not an admin or editor.
	 */
	public function enqueue() {
		if ( current_user_can( 'unfiltered_html' ) ) {
			return;
		}
		$tracking_id = $this->get_tracking_id();
		if ( ! $tracking_id ) {
			return;
		}
		$minify = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
		wp_enqueue_script( 'google-tag-manager', "https://www.googletagmanager.com/gtag/js?id={$tracking_id}", array(), '1', false );
		wp_enqueue_script( 'sixtenpress-google-analytics', plugins_url( "includes/js/tag-manager{$minify}.js", dirname( __FILE__ ) ), 'google-tag-manager', '0.1.0', false );
		wp_localize_script( 'sixtenpress-google-analytics', 'SixTenPressGA', array( 'id' => $tracking_id ) );
	}

	/**
	 * Async the Google Tag Manager script.
	 *
	 * @param $tag
	 * @param $handle
	 *
	 * @return mixed
	 */
	public function async( $tag, $handle ) {
		// Just return the tag normally if this isn't one we want to async
		if ( 'google-tag-manager' !== $handle ) {
			return $tag;
		}

		return str_replace( ' src', ' async src', $tag );
	}

	/**
	 * Get the Google Analytics tracking ID.
	 *
	 * @return mixed
	 */
	private function get_tracking_id() {
		if ( isset( $this->tracking_id ) ) {
			return $this->tracking_id;
		}
		$setting = get_option(
			'sixtenpressgoogleanalytics',
			array(
				'tracking_id' => '',
			)
		);
		if ( ! empty( $setting['tracking_id'] ) ) {
			$this->tracking_id = $setting['tracking_id'];
		}

		return apply_filters( 'sixtenpressgoogleanalytics_tracking_id', $this->tracking_id );
	}
}
