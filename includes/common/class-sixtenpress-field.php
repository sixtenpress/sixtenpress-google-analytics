<?php

/**
 * Class SixTenPressField
 * @copyright 2019 Robin Cornett
 */
class SixTenPressField {

	/**
	 * The custom field or setting prefix.
	 *
	 * @var string
	 */
	protected $prefix;

	/**
	 * Whether the fields being loaded have a repeater field.
	 *
	 * @var bool
	 */
	private $has_repeater;

	/**
	 * The assets class.
	 *
	 * @var \SixTenPressFieldAssets
	 */
	private $assets;

	/**
	 * SixTenPressField constructor.
	 */
	public function __construct() {
		$assets = $this->get_assets_class();
		add_action( 'admin_enqueue_scripts', array( $assets, 'load_styles' ) );
	}

	/**
	 * @param array  $field
	 * @param string $name
	 * @param string $id
	 * @param mixed  $value
	 * @param $getter
	 * @param bool   $group
	 */
	protected function pick_field( $field, $name, $id, $value, $getter, $group = false ) {
		$this->repeaters( ! empty( $field['repeatable'] ) );
		include_once plugin_dir_path( __FILE__ ) . 'fields/class-sixtenpress-field-base.php';
		$slug = str_replace( '_', '-', $field['type'] );
		$file = plugin_dir_path( __FILE__ ) . "fields/class-sixtenpress-field-{$slug}.php";
		if ( file_exists( $file ) ) {
			$field['required'] = $this->required( $field );
			if ( ! class_exists( 'SixTenPressAutoloader' ) ) {
				include_once $file;
			}
			$class = 'SixTenPressField' . ucfirst( $field['type'] );
			if ( 'checkbox_array' === $field['type'] ) {
				$class = 'SixTenPressFieldCheckboxArray';
			}
			$init = false;
			if ( class_exists( $class ) && is_callable( $class, 'do_field' ) ) {
				if ( in_array( $field['type'], array( 'checkbox_array', 'multiselect', 'select' ), true ) ) {
					$field['options'] = $this->get_options( $field );
				}
				$init = new $class( $name, $id, $value, $field );
			}
			if ( $init ) {
				switch ( $field['type'] ) {
					case 'file':
						call_user_func( array( $init, 'do_field' ), $getter, $group );
						break;

					default:
						call_user_func( array( $init, 'do_field' ) );
				}
				return;
			}
		}
		$method = "do_field_{$field['type']}";
		if ( is_callable( array( $this, $method ) ) ) {
			call_user_func( array( $this, $method ), $name, $id, $value, $field );

			return;
		}
	}

	/**
	 * If it's needed, enqueue the repeater script.
	 *
	 * @param boolean $is_repeatable
	 * @since 2.6.0
	 */
	protected function repeaters( $is_repeatable ) {
		if ( $this->has_repeater ) {
			return;
		}
		if ( ! $is_repeatable ) {
			return;
		}
		$assets = $this->get_assets_class();
		$assets->repeaters();
		$this->has_repeater = true;
	}

	/**
	 * If a field is required, this outputs the markup for it.
	 *
	 * @param $args
	 *
	 * @return string
	 */
	protected function required( $args ) {
		return empty( $args['required'] ) ? '' : ' required';
	}

	/**
	 * Helper function to get the array of options/choices
	 * for multiselect, checkbox array, and select fields.
	 *
	 * @since 2.0.0
	 *
	 * @param $args
	 *
	 * @return array
	 */
	protected function get_options( $args ) {
		$options = isset( $args['options'] ) ? $args['options'] : array();
		if ( isset( $args['choices'] ) ) {
			$options = $args['choices'];
		}
		$screen = get_current_screen();
		if ( 'post' !== $screen->base && ( is_string( $options ) ) ) {
			$function = "pick_{$options}";
			if ( method_exists( $this, $function ) ) {
				$options = $this->$function();
			}
		}
		if ( is_callable( $options ) ) {
			$options = call_user_func( $options );
		}

		return $options;
	}

	/**
	 * Generic callback to display a field description.
	 *
	 * @param  array $field setting used to identify description callback
	 *
	 * @since 1.0.0
	 */
	protected function do_description( $field ) {
		$id          = empty( $field['id'] ) ? $field['setting'] : $field['id'];
		$description = apply_filters( "sixtenpress_settings_{$this->prefix}_{$id}_description", $this->get_description( $field ), $field );
		if ( ! $description ) {
			return;
		}
		printf( '<p class="description">%s</p>', wp_kses_post( $description ) );
	}

	/**
	 * Get the field description somehow.
	 * @param $field
	 *
	 * @return mixed|string
	 */
	private function get_description( $field ) {
		$description = empty( $field['description'] ) ? false : $field['description'];
		if ( is_callable( $description ) ) {
			$description = call_user_func( $description );
		}
		if ( ! $description && ! empty( $field['id'] ) ) {
			$function = "{$field['id']}_description";
			if ( method_exists( $this, $function ) ) {
				$description = $this->$function();
			}
		}

		return $description;
	}

	/**
	 * Include and instantiate the fields assets class.
	 *
	 * @return object \SixTenPressFieldAssets
	 * @since 2.6.0
	 */
	private function get_assets_class() {
		if ( isset( $this->assets ) ) {
			return $this->assets;
		}
		include_once 'class-sixtenpress-field-assets.php';
		$this->assets = new SixTenPressFieldAssets();

		return $this->assets;
	}
}
