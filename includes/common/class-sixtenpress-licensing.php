<?php

/**
 * Generic licensing class to work with EDD Software Licensing.
 * @copyright 2016-2020 Robin Cornett
 */
class SixTenPressLicensing extends SixTenPressSettings {

	/**
	 * Current plugin version
	 * @var string $version
	 */
	public $version;

	/**
	 * Key for plugin setting base
	 * @var string
	 */
	protected $key;

	/**
	 * License key
	 * @var $license
	 */
	protected $license;

	/** License status
	 * @var $status
	 */
	protected $status;

	/**
	 * License data for this site (expiration date, latest version)
	 * @var $data
	 */
	protected $data;

	/**
	 * Store URL for Easy Digital Downloads.
	 * @var string
	 */
	protected $url = 'https://robincornett.com/edd-sl-api/';

	/**
	 * Plugin name for EDD.
	 * @var string
	 */
	protected $name;

	/**
	 * Plugin slug for license check.
	 * @var string
	 */
	protected $slug;

	/**
	 * The current plugin's basename.
	 * @var $basename
	 */
	protected $basename;

	/**
	 * The plugin's post ID in the store.
	 *
	 * @var integer
	 */
	protected $item_id;

	/**
	 * The plugin author.
	 * @var $author
	 */
	protected $author = 'Robin Cornett';

	/**
	 * Action for our custom nonce.
	 * @var string $action
	 */
	protected $action = 'sixtenpress_save-settings';

	/**
	 * Custom nonce.
	 * @var string $nonce
	 */
	protected $nonce = 'sixtenpress_nonce';

	/**
	 * Default licensing tab.
	 *
	 * @var string
	 */
	protected $tab = 'licensing';

	/**
	 * Set up EDD licensing updates
	 * @since 1.4.0
	 */
	public function updater() {

		if ( is_multisite() && ! is_main_site() ) {
			return;
		}

		if ( ! class_exists( 'EDD_SL_Plugin_Updater' ) ) {
			// load our custom updater if it doesn't already exist
			include plugin_dir_path( __FILE__ ) . 'class-eddpluginupdater.php';
		}

		$edd_updater = new EDD_SL_Plugin_Updater(
			$this->url,
			$this->basename,
			$this->get_updater_args()
		);

		$this->activate_license();
		$this->deactivate_license();
	}

	/**
	 * Get the parameters to send to the updater class.
	 *
	 * @return array
	 */
	private function get_updater_args() {
		return array(
			'version'   => $this->version,
			'license'   => trim( $this->get_license_key() ),
			'item_name' => $this->name,
			'author'    => $this->author,
			'url'       => home_url(),
			'item_id'   => $this->item_id,
		);
	}

	/**
	 * Register plugin license settings and fields
	 * @since 1.0.0
	 */
	public function register_settings() {
		$options_group = $this->is_sixten_active() ? "{$this->page}licensing" : $this->page;
		register_setting( $options_group, "{$this->key}_key", array( $this, 'sanitize_license' ) );
	}

	/**
	 * Register the licensing section.
	 * @return array
	 */
	protected function register_section() {
		return array(
			'licensing' => array(
				'id'    => 'licensing',
				'tab'   => 'licensing',
				'title' => __( 'Six/Ten Press License Key(s)', 'sixtenpress' ),
			),
		);
	}

	/**
	 * Register the license key field.
	 * @return array
	 */
	protected function register_fields() {
		return array(
			array(
				'id'       => "{$this->key}_key",
				'title'    => $this->name,
				'callback' => 'do_license_key_field',
				'section'  => 'licensing',
				'label'    => __( 'Enter your license key.', 'sixtenpress' ),
				'args'     => array(
					'status' => $this->get_license_status(),
				),
			),
		);
	}

	/**
	 * License key input field
	 *
	 * @param  array $field parameters to define field
	 *
	 * @since 1.4.0
	 */
	public function do_license_key_field( $field ) {
		if ( ! class_exists( 'SixTenPressAutoloader' ) ) {
			include_once 'fields/class-sixtenpress-field-license.php';
		}
		$output = new SixTenPressFieldLicense(
			$this->key, // $name
			$field['id'], // $id
			$this->get_license_key(), // $value
			$field // $field
		);
		$output->do_field();
	}

	/**
	 * Get the license status.
	 *
	 * @return string
	 * @since 2.5.0
	 */
	protected function get_license_status() {
		if ( isset( $this->status ) ) {
			return $this->status;
		}
		$this->status = get_option( "{$this->key}_status", false );

		return $this->status;
	}

	/**
	 * Get the license key.
	 *
	 * @return mixed|string
	 * @since 2.5.0
	 */
	protected function get_license_key() {
		if ( isset( $this->license ) ) {
			return $this->license;
		}
		$this->license = get_option( "{$this->key}_key", false );

		return $this->license;
	}

	/**
	 * Get the license data.
	 *
	 * @return mixed|array
	 * @since 2.5.0
	 */
	protected function get_license_data() {
		if ( isset( $this->data ) ) {
			return $this->data;
		}
		$this->data = get_option( "{$this->key}_data", false );

		return $this->data;
	}

	/**
	 * Sanitize license key
	 *
	 * @param  string $new_value license key
	 *
	 * @return string license key
	 *
	 * @since 1.4.0
	 */
	public function sanitize_license( $new_value ) {
		$license = get_option( "{$this->key}_key" );
		if ( ( $license && $license !== $new_value ) || empty( $new_value ) ) {
			delete_option( "{$this->key}_status" );
		}
		if ( $license !== $new_value || 'valid' !== $this->get_license_status() ) {
			$this->activate_license( $new_value );
		}

		return sanitize_text_field( $new_value );
	}

	/**
	 * Activate plugin license
	 *
	 * @param  string $new_value entered license key
	 *
	 * @uses  do_remote_request()
	 *
	 * @since 1.4.0
	 */
	protected function activate_license( $new_value = '' ) {

		if ( 'valid' === $this->get_license_status() ) {
			return;
		}

		// listen for our activate button to be clicked
		if ( empty( $_POST['sixtenpress_activate'] ) ) {
			return;
		}

		// If the user doesn't have permission to save, then display an error message
		if ( ! $this->user_can_save( $this->action, $this->nonce ) ) {
			wp_die( esc_attr__( 'Something unexpected happened. Please try again.', 'sixtenpress' ) );
		}

		// run a quick security check
		if ( ! check_admin_referer( $this->action, $this->nonce ) ) {
			return; // get out if we didn't click the Activate button
		}

		// retrieve the license from the database
		$license = trim( $this->get_license_key() );
		$license = $new_value !== $license ? trim( $new_value ) : $license;

		if ( empty( $license ) || empty( $new_value ) ) {
			delete_option( "{$this->key}_status" );

			return;
		}

		// data to send in our API request
		$api_params   = array(
			'edd_action' => 'activate_license',
			'license'    => $license,
			'item_name'  => rawurlencode( $this->name ), // the name of our product in EDD
			'url'        => esc_url( home_url() ),
		);
		$license_data = $this->do_remote_request( $api_params );
		$status       = 'invalid';
		if ( $license_data ) {
			$status = $license_data->license;
			if ( false === $license_data->success ) {
				$status = $license_data->error;
			}
		}

		// $license_data->license will be either "valid" or "invalid"
		update_option( "{$this->key}_status", $status );
	}

	/**
	 * Deactivate license
	 * @uses  do_remote_request()
	 *
	 * @since 1.4.0
	 */
	protected function deactivate_license() {

		// listen for our activate button to be clicked
		if ( empty( $_POST[ "{$this->key}_deactivate" ] ) ) {
			return;
		}

		// If the user doesn't have permission to save, then display an error message
		if ( ! $this->user_can_save( $this->action, $this->nonce ) ) {
			wp_die( esc_attr__( 'Something unexpected happened. Please try again.', 'sixtenpress' ) );
		}

		// run a quick security check
		if ( ! check_admin_referer( $this->action, $this->nonce ) ) {
			return; // get out if we didn't click the Activate button
		}

		// retrieve the license from the database
		$license = trim( $this->get_license_key() );

		// data to send in our API request
		$api_params   = array(
			'edd_action' => 'deactivate_license',
			'license'    => $license,
			'item_name'  => rawurlencode( $this->name ), // the name of our product in EDD
			'url'        => home_url(),
		);
		$license_data = $this->do_remote_request( $api_params );

		// $license_data->license will be either "deactivated" or "failed"
		if ( is_object( $license_data ) && 'deactivated' === $license_data->license ) {
			delete_option( "{$this->key}_status" );
		}
	}

	/**
	 * Weekly cron job to compare activated license with the server.
	 * @uses  check_license()
	 * @since 2.0.0
	 */
	public function weekly_license_check() {
		if ( apply_filters( 'sixtenpress_skip_license_check', false ) ) {
			return;
		}

		if ( ! empty( $_POST[ $this->nonce ] ) ) {
			return;
		}

		$license = get_option( $this->page . '_key', '' );
		if ( empty( $license ) ) {
			delete_option( "{$this->key}_status" );

			return;
		}

		$license_data = $this->check_license( $license );
		$status       = 'invalid';
		if ( $license_data ) {
			$status = $license_data->license;
			if ( false === $license_data->success ) {
				$status = $license_data->error;
			}
			$this->update_data_option( $license_data );
		}
		if ( $status !== $this->get_license_status() ) {
			// Update local plugin status
			update_option( "{$this->key}_status", $status );
		}
	}

	/**
	 * Update the plugin data setting.
	 *
	 * @param $license_data
	 */
	protected function update_data_option( $license_data ) {
		$data_setting = "{$this->key}_data";
		$class_data   = $this->get_license_data();
		if ( ! isset( $class_data['expires'] ) || $license_data->expires !== $class_data['expires'] ) {
			$this->update_settings(
				array(
					'expires' => $license_data->expires,
				),
				$data_setting
			);
		}

		if ( 'valid' === $license_data->license ) {
			return;
		}

		$latest_version = $this->get_latest_version();
		if ( ! isset( $class_data['latest_version'] ) || $latest_version !== $class_data['latest_version'] ) {
			$this->update_settings(
				array(
					'latest_version' => $latest_version,
				),
				$data_setting
			);
		}
	}

	/**
	 * Check plugin license status
	 * @uses  do_remote_request()
	 *
	 * @param string $license
	 *
	 * @return mixed data
	 ** @since 1.4.0
	 */
	protected function check_license( $license = '' ) {

		$api_params = array(
			'edd_action' => 'check_license',
			'license'    => empty( $license ) ? $this->get_license_key() : $license,
			'item_name'  => rawurlencode( $this->name ), // the name of our product in EDD
			'url'        => esc_url( home_url() ),
		);
		if ( empty( $api_params['license'] ) ) {
			return '';
		}

		return $this->do_remote_request( $api_params );
	}

	/**
	 * Get the latest plugin version.
	 * @uses  do_remote_request()
	 * @return mixed
	 *
	 * @since 2.0.0
	 */
	protected function get_latest_version() {
		$api_params = array(
			'edd_action' => 'get_version',
			'item_name'  => $this->name,
			'slug'       => $this->slug,
		);
		$request    = $this->do_remote_request( $api_params );

		if ( $request && isset( $request->sections ) ) {
			$request->sections = maybe_unserialize( $request->sections );
		} else {
			return false;
		}

		return $request->new_version;
	}

	/**
	 * Send the request to the remote server.
	 *
	 * @param $api_params
	 *
	 * @param int $timeout
	 *
	 * @return array|bool|mixed|object
	 * @since 2.0.0
	 */
	private function do_remote_request( $api_params, $timeout = 15 ) {
		$response = wp_remote_post(
			$this->url,
			array(
				'timeout'   => $timeout,
				'sslverify' => false,
				'body'      => $api_params,
			)
		);
		if ( is_wp_error( $response ) || 200 !== wp_remote_retrieve_response_code( $response ) ) {
			return false;
		}

		return json_decode( wp_remote_retrieve_body( $response ) );
	}

	/**
	 * Print error messages.
	 *
	 * @since 1.4.0
	 *
	 * @param $message
	 * @param string $class
	 */
	protected function do_error_message( $message, $class = '' ) {
		if ( empty( $message ) ) {
			return;
		}
		printf( '<div class="notice %s">%s</div>', esc_attr( $class ), wp_kses_post( $message ) );
	}

	/**
	 * Convert a date string to a pretty format.
	 *
	 * @param $args
	 * @param string $before
	 * @param string $after
	 *
	 * @return string
	 */
	protected function pretty_date( $args, $before = '', $after = '' ) {
		$date_format = isset( $args['date_format'] ) ? $args['date_format'] : get_option( 'date_format' );

		return $before . date_i18n( $date_format, $args['field'] ) . $after;
	}

	/**
	 * Boolean to trigger the default 6/10 press error message.
	 *
	 * @param $error
	 *
	 * @return bool
	 * @since 1.1.2
	 */
	public function sixtenpress_error_message( $error ) {
		return 'valid' === $this->get_license_status() ? $error : true;
	}
}
