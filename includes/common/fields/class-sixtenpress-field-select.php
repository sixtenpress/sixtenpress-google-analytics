<?php

/**
 * Build out the select field.
 *
 * Class SixTenPressFieldSelect
 * @copyright 2018-2020 Robin Cornett
 */
class SixTenPressFieldSelect extends SixTenPressFieldBase {

	/**
	 * Build a select field.
	 */
	public function do_field() {
		printf(
			'<select id="%1$s" name="%2$s" aria-label="%5$s"%3$s%4$s>',
			esc_attr( $this->id ),
			esc_attr( $this->name ),
			$this->get_class( $this->field ),
			esc_attr( $this->field['required'] ),
			esc_attr( $this->get_aria( $this->field ) )
		);
		foreach ( (array) $this->field['options'] as $option => $field_label ) {
			printf(
				'<option value="%s" %s>%s</option>',
				esc_attr( $option ),
				selected( $option, $this->value, false ),
				esc_attr( $field_label )
			);
		}
		echo '</select>';
	}

	/**
	 * Define the optional class for the select field.
	 *
	 * @return string
	 */
	private function get_class() {
		if ( $this->do_select_script() ) {
			$this->load_scripts_styles();
			$this->field['class'] .= ' enhanced';
		}
		return empty( $this->field['class'] ) ? '' : sprintf( ' class="%s"', $this->field['class'] );
	}

	/**
	 * Define the aria-label value for the select field.
	 *
	 * @return string
	 */
	private function get_aria() {
		$aria = $this->name;
		if ( isset( $this->field['label'] ) ) {
			$aria = $this->field['label'];
		} elseif ( isset( $this->field['title'] ) ) {
			$aria = $this->field['title'];
		}

		return $aria;
	}

	/**
	 * If a select field is enhanced, enqueue Select2 script/style.
	 *
	 * @since 2.6.0
	 */
	private function load_scripts_styles() {
		$select2_version = '4.0.12';
		wp_enqueue_style( 'select2', plugins_url( '/css/select2.css', dirname( __DIR__ ) ), array( 'sixtenpress-postmeta' ), $select2_version, 'screen' );
		wp_enqueue_script( 'select2', plugins_url( '/js/select2.min.js', dirname( __DIR__ ) ), array( 'jquery' ), $select2_version, 'screen' );
		wp_add_inline_script( 'select2', 'jQuery(document).ready(function($) {$(\'.sixten-meta select.enhanced\').select2();});' );
	}

	/**
	 * Should the select field have the Select2 script run on it?
	 *
	 * @return boolean
	 * @since 2.6.0
	 */
	private function do_select_script() {
		return ! empty( $this->field['enhanced'] );
	}
}
