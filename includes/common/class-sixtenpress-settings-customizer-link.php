<?php

/**
 * A class to get a customizer link for a section/settings page.
 *
 * class SixTenPressSettingsCustomizerLink
 */
class SixTenPressSettingsCustomizerLink {

	/**
	 * The instance of the class.
	 *
	 * @var object
	 */
	private static $instance;

	/**
	 * Return an instance of this class.
	 *
	 * @return object
	 */
	public static function instance() {
		if ( ! isset( self::$instance ) && ! ( self::$instance instanceof SixTenPressSettingsCustomizerLink ) ) {
			self::$instance = new SixTenPressSettingsCustomizerLink();
		}

		return self::$instance;
	}

	/**
	 * Get a customizer link.
	 *
	 * @param  string $section Optional section to focus on. Panel is assumed to be sixtenpress.
	 * @return string
	 */
	public function get( $section = '' ) {
		return sprintf(
			' <a class="page-title-action hide-if-no-customize" href="%1$s">%2$s</a>',
			esc_url(
				add_query_arg(
					array(
						array(
							'autofocus' => $this->get_autofocus( $section ),
						),
						'return' => rawurlencode( remove_query_arg( wp_removable_query_args(), wp_unslash( $_SERVER['REQUEST_URI'] ) ) ),
					),
					admin_url( 'customize.php' )
				)
			),
			esc_html__( 'Manage with Live Preview', 'sixtenpress' )
		);
	}

	/**
	 * Define the autofocus panel, and optionally section, for the Customizer.
	 *
	 * @param string $section
	 * @return array
	 */
	private function get_autofocus( $section = '' ) {
		$autofocus = array(
			'panel' => 'sixtenpress',
		);
		if ( $section ) {
			$autofocus['section'] = $section;
		}

		return $autofocus;
	}
}

/**
 * Helper function to get the customizer link.
 *
 * @return object
 */
function sixtenpress_customizer_link() {
	return SixTenPressSettingsCustomizerLink::instance();
}
