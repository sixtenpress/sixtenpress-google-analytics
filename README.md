# Six/Ten Press Google Analytics

_Six/Ten Press Google Analytics_ is a wee plugin to add Google Tag Manager/Google Analytics quickly and easily to your website.

## Installation

### Manual

1. Download the latest tagged archive (choose the "zip" option).
2. Unzip the archive.
3. Copy the folder to your `/wp-content/plugins/` directory.
4. Go to the Plugins screen and click __Activate__.

Check out the Codex for more information about [installing plugins manually](https://codex.wordpress.org/Managing_Plugins#Manual_Plugin_Installation).

### Git

Using git, browse to your `/wp-content/plugins/` directory and clone this repository:

`git clone git@gitlab.com:sixtenpress/sixtenpress-google-analytics.git`

Then go to your Plugins screen and click __Activate__.

## Screenshots

## Frequently Asked Questions

## Changelog

### 0.1.0
* Initial release
