<?php

/**
 * Six/Ten Press Featured Content Blocks
 * @package SixTenPressFeaturedContentBlocks
 * @author  Robin Cornett
 * @license GPL-2.0+
 *
 * @wordpress-plugin
 * Plugin Name: Six/Ten Press Google Analytics
 * Plugin URI:  https://gitlab.com/sixtenpress/sixtenpress-google-analytics
 * Description: A small plugin to add Google Analytics tracking to your site.
 * Version:     0.1.0
 * Author:      Robin Cornett
 * Author URI:  https://robincornett.com/
 * Text Domain: sixtenpress-google-analytics
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path: /languages
 */

// if this file is called directly abort
if ( ! defined( 'WPINC' ) ) {
	die;
}

function sixtenpressgoogleanalytics_require() {
	$files = array(
		'class-sixtenpressgoogleanalytics',
	);
	foreach ( $files as $file ) {
		require plugin_dir_path( __FILE__ ) . 'includes/' . $file . '.php';
	}
}

sixtenpressgoogleanalytics_require();
new SixTenPressGoogleAnalytics();
