'use strict';

var gulp = require( 'gulp' );

gulp.task( 'build', [
	'js',
	'translate',
	'zip'
] );
